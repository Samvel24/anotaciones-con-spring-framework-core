package com.samuel.pruebaannotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
/*
Configuración para que Spring escanee todas las clases que realicemos y busque en ellas 
las anotaciones de Java. En los paréntesis de la anotación debemos poner el paquete en el 
que deseamos que se realice la busqueda de anotaciones.
*/
@ComponentScan("com.samuel.pruebaannotations")
public class EmpleadosConfig {
	// Definir el bean para InformeFinancieroDtoCompras
	@Bean
	// El nombre para este método será el id del Bean inyectado
	public CreacionInformeFinanciero infomeFinancieroDtoCompras() {
		return new InformeFinancieroDtoCompras();
	}
	
	// Definir el bean para DirectorFinanciero e inyectar dependencias
	@Bean
	public Empleado directorFinanciero() {
		return new DirectorFinanciero(infomeFinancieroDtoCompras());
	}
}
