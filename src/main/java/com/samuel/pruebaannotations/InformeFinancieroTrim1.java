package com.samuel.pruebaannotations;

import org.springframework.stereotype.Component;

/* 
Usamos @Component para que Spring registre automáticamente este Bean
*/
@Component
public class InformeFinancieroTrim1 implements CreacionInformeFinanciero {
	@Override
	public String getInformeFinanciero() {
		// TODO Auto-generated method stub
		return "Presentación de informe financiero del trimestre 1";
	}
}
