package com.samuel.pruebaannotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Leemos el XML de configuración con ClassPathXmlApplicationContext
		// ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("com/samuel/pruebaannotations/ApplicationContext.xml");
		
		// Leemos el archivo .class de configuración con AnnotationConfigApplicationContext
		AnnotationConfigApplicationContext contexto = new AnnotationConfigApplicationContext(EmpleadosConfig.class);
		
		/*
		Empleado Antonio = contexto.getBean("comercial", Empleado.class);
		System.out.println(Antonio.getInforme());
		System.out.println(Antonio.getTareas());
		*/
		
		Empleado empleado = contexto.getBean("directorFinanciero", Empleado.class);
		System.out.println(empleado.getTareas());
		System.out.println(empleado.getInforme());
		
		contexto.close();
	}

}
