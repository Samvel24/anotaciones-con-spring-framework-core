package com.samuel.pruebaannotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("com/samuel/pruebaannotations/ApplicationContext.xml");
		Empleado Antonio = contexto.getBean("comercial", Empleado.class);
		Empleado Lucia = contexto.getBean("comercial", Empleado.class);
		
		// Imprimimos en pantalla los códigos hash de cada objeto
		System.out.println(Antonio);
		System.out.println(Lucia);
		contexto.close();
	}
}
