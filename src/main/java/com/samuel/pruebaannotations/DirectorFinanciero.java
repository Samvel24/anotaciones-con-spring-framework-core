package com.samuel.pruebaannotations;

public class DirectorFinanciero implements Empleado {
	
	private CreacionInformeFinanciero creacionInforme;

	public DirectorFinanciero(CreacionInformeFinanciero creacionInforme) {
		super();
		this.creacionInforme = creacionInforme;
	}

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestión y dirección de las operaciones financieras de la empresa";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return creacionInforme.getInformeFinanciero();
	}

}
