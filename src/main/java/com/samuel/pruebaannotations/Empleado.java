package com.samuel.pruebaannotations;

public interface Empleado {
	public String getTareas();
	public String getInforme();
}
