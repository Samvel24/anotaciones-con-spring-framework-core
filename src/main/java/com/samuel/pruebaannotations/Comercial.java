package com.samuel.pruebaannotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/* 
En la siguiente anotación podemos colocar entre paréntesis un id asignado a este Bean, 
con esto, Spring registra automáticamente este Bean.
*/
@Component
@Scope("singleton")
public class Comercial implements Empleado {
	// Esta clase necesita una dependencia y es un informe financiero
	//@Autowired
	private CreacionInformeFinanciero nuevoInforme;
	
	@PostConstruct
	public void ejecutaAntesDeCreacionDeBean() {
		System.out.println("Ejecutado antes de la creación del Bean");
	}
	
	@PreDestroy
	public void ejecutaDespuesDeApagadoElContenedor() {
		System.out.println("Ejecutado despues de apagado el contenedor");
	}
	
	/*
	Cuando usamos la anotación @Autowired, lo que hace Spring es buscar en todo el 
	proyecto una clase que implemente la interface CreacionInformeFinanciero, si 
	encuentra esa clase, entonces de ella obtiene la dependencia requerida y la inyecta
	al constructor que hemos definido, con esto, podemos usar esa dependencia al 
	interior de esta clase y llamar a los métodos de la clase que implementa la interface
	que hemos mencionado.
	*/
	//@Autowired
	public Comercial(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}
	
	// Este constructor no lleva anotación porque no se usa para inyectar dependencias
	public Comercial() {
		// Constructor vacío
	}
	
	@Autowired
	@Qualifier("informeFinancieroTrim2")
	public void setNuevoInforme(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "¡Vender, vender y vender mas!";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return nuevoInforme.getInformeFinanciero();
	}
}
